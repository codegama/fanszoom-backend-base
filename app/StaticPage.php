<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StaticPage extends Model
{
    /**
     * Scope a query to only include active users.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeCommonResponse($query) {

        return $query->select('static_pages.*', 'static_pages.id as static_page_id', 'static_pages.unique_id as static_page_unique_id');
    
    }
}
