<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MeetingsRelatedMigrations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
            if(!Schema::hasTable('meetings')) {

                Schema::create('meetings', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('unique_id')->default(rand());
                    $table->integer('user_id');
                    $table->string('title');
                    $table->text('description')->nullable();
                    $table->string('picture')->default(asset('meeting-placeholder.jpg'));
                    $table->string('meeting_type')->default(MEETING_TYPE_LIVE);
                    $table->string('video')->default('');
                    $table->dateTime('schedule_time')->nullable();
                    $table->dateTime('start_time')->nullable();
                    $table->dateTime('end_time')->nullable();
                    $table->integer('no_of_users')->default(1);
                    $table->float('no_of_minutes')->default(1)->comment("in minutes");
                    $table->string('created_by')->default('user');
                    $table->tinyInteger('is_cancelled')->default(0);
                    $table->text('cancelled_reason')->nullable();
                    $table->tinyInteger('status')->default(1);
                    $table->string('bbb_internal_meeting_id')->default("");
                    $table->string('bbb_voice_bridge')->default("");
                    $table->string('bbb_dial_number')->default("");
                    $table->string('bbb_record_id')->default("");
                    $table->tinyInteger('is_recordings')->default(NO);
                    $table->string('recording_url')->default("");
                    $table->timestamps();
                });

            }


            if(!Schema::hasTable('meeting_users')) {

                Schema::create('meeting_users', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('unique_id')->default(rand());
                    $table->integer('meeting_id');
                    $table->integer('user_id')->default(0);
                    $table->string('username');
                    $table->dateTime('start_time');
                    $table->dateTime('end_time')->nullable();
                    $table->tinyInteger('status')->default(1);
                    $table->timestamps();
                });

            }

            if(!Schema::hasTable('meeting_records')) {

                Schema::create('meeting_records', function (Blueprint $table) {
                    $table->bigIncrements('id');
                    $table->string('unique_id')->default(rand());
                    $table->integer('meeting_id');
                    $table->integer('user_id');
                    $table->string('playback_url');
                    $table->string('overall_record_url')->default("");
                    $table->string('video_record_url')->default("");
                    $table->time('duration')->default('00:00:00');
                    $table->string('no_of_views')->default(0);
                    $table->tinyInteger('status')->default(1);
                    $table->string('bbb_record_id')->default("");
                    $table->timestamps();
                });

            }


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('meetings');
        Schema::dropIfExists('meeting_users');
        Schema::dropIfExists('meeting_records');
    }
}
