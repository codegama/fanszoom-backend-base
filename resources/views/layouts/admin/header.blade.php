<div class="navbar navbar-expand flex-column flex-md-row navbar-custom">
    <div class="container-fluid">
        <!-- LOGO -->
        <a href="{{route('admin.dashboard')}}" class="navbar-brand mr-0 mr-md-2 logo">
            <span class="logo-lg">
                <img src="{{Setting::get('site_icon')}}" alt="" height="24" />
                <!-- <span class="d-inline h5 ml-1 text-logo">{{Setting::get('site_name')}}</span> -->
            </span>
            <span class="logo-sm">
                <img src="{{Setting::get('site_icon')}}" alt="" height="24">
            </span>
        </a>

        <ul class="navbar-nav bd-navbar-nav flex-row list-unstyled menu-left mb-0">
            <li class="">
                <button class="button-menu-mobile open-left disable-btn">
                    <i data-feather="menu" class="menu-icon"></i>
                    <i data-feather="x" class="close-icon"></i>
                </button>
            </li>
        </ul>

        <ul class="navbar-nav flex-row ml-auto d-flex list-unstyled topnav-menu float-right mb-0">

            <li class="dropdown align-self-center">
                <a class="nav-link dropdown-toggle nav-user mr-0" data-toggle="dropdown" href="#" role="button"
                    aria-haspopup="false" aria-expanded="false">
                    <div class="media user-profile ">
                        <img src="{{Auth::guard('admin')->user()->picture}}" alt="user-image" class="rounded-circle align-self-center" />
                        <div class="media-body text-left">
                            <h6 class="pro-user-name ml-2 my-0">
                                <span>{{Auth::guard('admin')->user()->name}}</span>
                                <span class="pro-user-desc text-muted d-block mt-1">Administrator </span>
                            </h6>
                        </div>
                        <span data-feather="chevron-down" class="ml-2 align-self-center"></span>
                    </div>
                </a>
                <div class="dropdown-menu profile-dropdown-items dropdown-menu-right">
                    <a href="{{route('admin.profile')}}" class="dropdown-item notify-item">
                        <i data-feather="user" class="icon-dual icon-xs mr-2"></i>
                        <span>{{tr('my_account')}}</span>
                    </a>

                    <a href="{{route('admin.settings')}}" class="dropdown-item notify-item">
                        <i data-feather="settings" class="icon-dual icon-xs mr-2"></i>
                        <span>{{tr('settings')}}</span>
                    </a>

                    <div class="dropdown-divider"></div>

                     <a class="dropdown-item notify-item" data-toggle="modal" data-target="#logoutModel">
                        <i data-feather="log-out" class="icon-dual icon-xs mr-2"></i>
                        <span>{{tr('logout')}}</span>
                    </a>

                </div>
            </li>
        </ul>
    
    </div>

</div>