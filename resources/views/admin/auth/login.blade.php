<!DOCTYPE html>

<html lang="en">
    
<head>
    <meta charset="utf-8" />

    <title>{{Setting::get('site_name')}}</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{Setting::get('site_icon')}}">

    <!-- App css -->
    <link href="{{asset('admin-assets/assets/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('admin-assets/assets/css/icons.min.css')}}" rel="stylesheet" type="text/css" />

    <link href="{{asset('admin-assets/assets/css/app.min.css')}}" rel="stylesheet" type="text/css" />

</head>

    <body class="authentication-bg">
        
        <div class="account-pages my-5">

            <div class="container">

                <div class="row justify-content-center">

                    <div class="col-xl-10">

                        <div class="card">

                            <div class="card-body p-0">

                                <div class="row">

                                    <div class="col-md-6 p-5">
                                        <div class="mx-auto mb-5">
                                            <a href="{{route('admin.login')}}">
                                                <img src="{{Setting::get('site_logo')}}" alt="{{Setting::get('site_name')}}" height="24" />
                                                <!-- <h3 class="d-inline align-middle ml-1 text-logo">{{Setting::get('site_name')}}</h3> -->
                                            </a>
                                        </div>

                                        @include('notifications.notify')

                                        <h6 class="h5 mb-0 mt-4">{{tr('welcome_back')}}</h6>
                                        <p class="text-muted mt-1 mb-4">{{tr('enter_admin_panel_email_and_password')}}</p>


                                        <form class="authentication-form" method="POST" action="{{route('admin.login.post')}}" autocomplete="new-password">
                                        @csrf

                                            <input type="hidden" name="timezone" id="userTimezone">
                                            
                                            <div class="form-group">

                                                <label class="form-control-label">{{tr('email')}}</label>
                                                <div class="input-group input-group-merge">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="icon-dual" data-feather="mail"></i>
                                                        </span>
                                                    </div>
                                                    
                                                    <input id="email" type="email" class="form-control" name="email" value="{{ Setting::get('demo_admin_email')?: old('email') }}" required autocomplete="email" autofocus placeholder="{{tr('enter_email')}}">
                                                </div>

                                            </div>

                                            <div class="form-group mt-4">
                                                <label class="form-control-label">{{tr('password')}}</label>
                                                
                                                <div class="input-group input-group-merge">
                                                    <div class="input-group-prepend">
                                                        <span class="input-group-text">
                                                            <i class="icon-dual" data-feather="lock"></i>
                                                        </span>
                                                    </div>
                                                    <input id="password" type="password" class=" form-control input100 @error('password') is-invalid @enderror" name="password" value="{{Setting::get('demo_admin_password') ?: old('password') }}" required autocomplete="password" autofocus placeholder="{{tr('enter_password')}}">
                                                </div>
                                            </div>

                                            <div class="form-group mb-0 text-center">
                                                <button class="btn btn-primary btn-block" type="submit"> {{tr('login')}}
                                                </button>
                                            </div>

                                        </form>
                                        
                                    </div>

                                    <div class="col-lg-6 d-none d-md-inline-block">

                                        <div class="auth-page-sidebar">

                                            <div class="overlay"></div>

                                            <div class="auth-user-testimonial">
                                                <p class="font-size-24 font-weight-bold text-white mb-1">{{Setting::get('site_name')}}</p>
                                                <p class="lead">{{Setting::get('tag_name')}}</p>
                                               
                                            </div>

                                        </div>

                                    </div>

                                </div>
                                
                            </div> 
                        </div>

                    </div> 

                </div>

            </div>

        </div>
       
        <script src="{{asset('admin-assets/assets/js/vendor.min.js')}}"></script>

        <script src="{{asset('admin-assets/assets/js/app.min.js')}}"></script>

        <script src="{{asset('admin-assets/assets/js/jstz.min.js')}}"></script>

        <script>
    
            $(document).ready(function() {

                var dMin = new Date().getTimezoneOffset();

                var dtz = -(dMin/60);
                console.log(dtz);
                $("#userTimezone").val(jstz.determine().name());
            });

        </script>
        
    </body>

</html>


                

