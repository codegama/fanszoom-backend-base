@extends('layouts.admin') 

@section('content-header', tr('meetings'))

@section('bread-crumb')

    <li class="breadcrumb-item"><a href="{{ route('admin.meetings.index')}}">{{tr('meetings')}}</a></li>

    @if(Request::get('type'))

        <li class="breadcrumb-item active" aria-current="page">
            <span>{{ tr('scheduled_meetings') }}</span>
        </li> 

    @else
        <li class="breadcrumb-item active" aria-current="page">
            <span>{{ tr('meetings_history') }}</span>
        </li> 
    @endif

@endsection 

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card">

            <div class="card-body">

                <div class="border-bottom pb-3">

                    <h5 class="text-uppercase">{{Request::get('type') ? tr('scheduled_meetings') : tr('meetings_history')}}</h5>

                </div>
                
                @include('admin.meetings._search')

                <table  id="basic-datatable" class="table dt-responsive nowrap">

                    <thead>

                        <tr>
                            <th>{{tr('s_no')}}</th>
                            <th>{{tr('unique_id')}}</th>
                            <th>{{tr('title')}}</th>
                            <th>{{tr('user')}}</th>
                            <th>{{tr('schedule_time')}}</th>
                            <th>{{tr('total_users')}}</th>
                            <th>{{tr('status')}}</th>
                            <th>{{tr('record')}}</th>
                            <th>{{tr('action')}}</th>
                        </tr>

                    </thead>

                    <tbody>

                        @foreach($meetings as $i => $meeting_details)
                             
                            <tr>
                                <td>{{$i+$meetings->firstItem()}}</td>

                                <td>
                                    <a href="{{route('admin.meetings.view',['meeting_id' => $meeting_details->id])}}"> {{ $meeting_details->unique_id }}
                                    </a>
                                </td>
                               
                                <td> 

                                <a href="{{route('admin.meetings.view',['meeting_id' => $meeting_details->id])}}">
                                    {{ $meeting_details->title }} 
                                </a>
                                </td>

                                <td>
                                    <a href="{{route('admin.users.view',['user_id' => $meeting_details->user_id])}}"> {{ $meeting_details->userDetails->name ?? "-" }}
                                    </a>
                                </td>

                                <td>
                                    {{common_date($meeting_details->schedule_time,Auth::guard('admin')->user()->timezone)}}

                                </td>
                               
                                <td>
                                    {{ $meeting_details->users_count }}
                                </td>

                                <td>

                                   <span class="badge badge-secondary"> {{$meeting_details->status_formatted}}</span>

                                </td>

                                <td>

                                   <span class="badge badge-info"> {{$meeting_details->is_recordings_formatted}}</span>

                                </td>

                                <td>     

                                    <div class="template-demo">

                                        <div class="dropdown">

                                            <button class="btn btn-outline-primary  dropdown-toggle" type="button" id="actionDropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                {{tr('action')}}
                                            </button>

                                            <div class="dropdown-menu" aria-labelledby="actionDropdown">
                                              
                                                <a class="dropdown-item" href="{{ route('admin.meetings.view', ['meeting_id' => $meeting_details->id]) }}">
                                                    {{tr('view')}}
                                                </a>

                                                @if($meeting_details->is_recordings == YES)

                                                <a class="dropdown-item" href="{{ route('admin.meeting_records.view', ['meeting_id' => $meeting_details->id]) }}">
                                                    {{tr('view_record')}}
                                                </a>

                                                @endif
                                                
                                                @if($meeting_details->status != MEETING_ENDED)
                                                    <a class="dropdown-item" href="{{ route('admin.meetings.edit', ['meeting_id' => $meeting_details->id]) }}">
                                                        {{tr('edit')}}
                                                    </a>
                                                    
                                                    <a class="dropdown-item" href="{{route('admin.meetings.delete', ['meeting_id' => $meeting_details->id])}}" 
                                                    onclick="return confirm(&quot;{{tr('meeting_delete_confirmation' , $meeting_details->title)}}&quot;);">
                                                        {{tr('delete')}}
                                                    </a>
                                                
                                                @endif

                                            </div>

                                        </div>

                                    </div>

                                </td>

                            </tr>

                        @endforeach
                        
                    </tbody>
                   
                </table>

                <div class="float-right">{{ $meetings->appends(request()->input())->links() }}</div>
                
            </div>
            
        </div>
       
    </div>
   
</div>


@endsection