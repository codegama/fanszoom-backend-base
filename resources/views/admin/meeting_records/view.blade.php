@extends('layouts.admin')

@section('content-header', tr('meeting_management'))

@section('bread-crumb')

<li class="breadcrumb-item"><a href="{{ route('admin.meetings.index')}}">{{tr('meeting_records')}}</a></li>

<li class="breadcrumb-item active" aria-current="page">
    <span>{{ tr('view_meeting_records') }}</span>
</li>

@endsection

@section('content')

<div class="row">

    <div class="col-12">

        <div class="card card-navy card-outline">

            <div class="card-body">

                <h5 class="text-uppercase border-bottom pb-3">{{tr('view_meeting_records')}}</h5>

                <div class="row">

                    <div class="col-md-6">

                        <div class="card card-widget widget-user-2">

                            <div class="video-frame">
                                <iframe id="player2" width="560" height="315" src="{{$meeting_record_details->playback_url ?: asset('meeting-placeholder.jpg')}}" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
                            </div>

                        </div>

                        <a class="btn btn-success" href="{{route('admin.meetings.view', ['meeting_id' => $meeting_record_details->meeting_id])}}">{{tr('view_meeting')}}</a>

                        <a class="btn btn-danger" onclick="return confirm(&quot;{{tr('meeting_records_delete_confirmation', $meeting_record_details->meeting->title)}}&quot;);" href="{{route('admin.meeting_records.delete', ['meeting_record_id' => $meeting_record_details->id])}}">{{tr('delete')}}</a>

                    </div>

                    <div class="col-md-6">

                        <div class="card">
                            <div class="card-body p-0">
                                <h5 class="card-title header-title border-bottom p-3 mb-0">{{tr('overview')}}</h5>
                                <!-- stat 1 -->
                                <div class="media px-3 py-4 border-bottom">
                                    <div class="media-body">
                                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{common_date($meeting_details->start_time, Auth::guard('admin')->user()->timezone)}}</h4>
                                        <span class="text-muted">{{tr('start_time')}}</span>
                                    </div>
                                    <i data-feather="clock" class="align-self-center icon-dual icon-lg"></i>
                                </div>
                                <!-- stat 2 -->
                                <div class="media px-3 py-4 border-bottom">
                                    <div class="media-body">
                                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{common_date($meeting_details->end_time, Auth::guard('admin')->user()->timezone)}}</h4>
                                        <span class="text-muted">{{tr('end_time')}}</span>
                                    </div>
                                    <i data-feather="clock" class="align-self-center icon-dual icon-lg"></i>
                                </div>

                                <!-- stat 3 -->
                                <div class="media px-3 py-4">
                                    <div class="media-body">
                                        <h4 class="mt-0 mb-1 font-size-22 font-weight-normal">{{common_date($meeting_details->schedule_time, Auth::guard('admin')->user()->timezone)}}</h4>
                                        <span class="text-muted">{{tr('schedule_time')}}</span>
                                    </div>
                                    <i data-feather="clock" class="align-self-center icon-dual icon-lg"></i>
                                </div>
                            </div>
                        
                        </div>

                        <div class="card card-widget widget-user-2 box-shadow-none">

                            <div class="card p-0 box-shadow-none">

                                <ul class="nav flex-column">

                                    <li class="nav-item">
                                        <div class="nav-link">{{tr('unique_id')}}<span class="float-right text-uppercase">{{$meeting_record_details->unique_id}}</span> </div>
                                    </li>

                                    <li class="nav-item">
                                        <div class="nav-link">{{tr('username')}} <a href="{{route('admin.users.view',['user_id' => $meeting_record_details->user_id])}}"><span class="float-right text-uppercase">{{ $meeting_record_details->userDetails->name ?? "-" }}</span></a></div>
                                    </li>

                                    <li class="nav-item">
                                        <div class="nav-link">{{tr('title')}} <span class="float-right text-uppercase">{{$meeting_record_details->meeting->title ?? ''}}</span> </div>
                                    </li>

                                    <li class="nav-item">
                                        <div class="nav-link">{{tr('created_by')}}<span class="float-right text-uppercase">{{$meeting_record_details->meeting->created_by ?? ''}}</span> </div>
                                    </li>

                                    <li class="nav-item">
                                        <div class="nav-link">{{tr('status')}}
                                            <span class="float-right badge badge-success text-uppercase">{{$meeting_record_details->meeting->status_formatted ?? ''}}</span>
                                        </div>
                                    </li>

                                    <li class="nav-item">
                                        <div class="nav-link">{{tr('created_at')}}<span class="float-right">{{common_date($meeting_record_details->created_at,Auth::guard('admin')->user()->timezone)}}</span></div>
                                    </li>

                                    <li class="nav-item">
                                        <div class="nav-link">{{tr('updated_at')}}<span class="float-right">{{common_date($meeting_record_details->updated_at,Auth::guard('admin')->user()->timezone)}}</span></div>
                                    </li>

                                </ul>

                            </div>

                        </div>

                    </div>

                </div>

            </div>

        </div>

    </div>

</div>
@endsection


@section('scripts')

<script type="text/javascript">
    const Player = document.getElementById('player2');
    const PlayBtn = document.getElementById('play');
    const stopBtn = document.getElementById('stop');
    let times = 0,
        playY;
    const playVideo = PlayBtn.addEventListener('click', () => {
        if (times == 0) {
            playY = Player.src += '?autoplay=1';
            times = 1;
        }
    });

    stopBtn.addEventListener('click', () => {
        playY = playY.slice(0, -11);
        Player.src = playY
        times = 0;
    });
</script>

@endsection