<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('user', function (Request $request) {
    return $request->user();
});

Route::get('/email/verify', 'ApplicationController@email_verify')->name('email.verify');

Route::group(['prefix' => 'user' , 'middleware' => 'cors'], function() {

    Route::any('pages_list' , 'ApplicationController@static_pages_api');

    Route::any('support_contacts_save' , 'ApplicationController@support_contacts_save');

    Route::get('get_settings_json', function () {

        if(\File::isDirectory(public_path(SETTINGS_JSON))){

        } else {

            \File::makeDirectory(public_path('default-json'), 0777, true, true);

            \App\Helpers\Helper::settings_generate_json();
        }

        $jsonString = file_get_contents(public_path('default-json/settings.json'));

        $data = json_decode($jsonString, true);

        return $data;
    
    });

	/***
	 *
	 * User Account releated routs
	 *
	 */

    Route::post('register','UserApi\AccountApiController@register');
    
    Route::post('login','UserApi\AccountApiController@login');

    Route::post('forgot_password', 'UserApi\AccountApiController@forgot_password');

    Route::post('reset_password', 'UserApi\AccountApiController@reset_password');


    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('profile','UserApi\AccountApiController@profile'); // 1

        Route::post('update_profile', 'UserApi\AccountApiController@update_profile'); // 2

        Route::post('update_profile_picture', 'UserApi\AccountApiController@update_profile_picture'); // 2

        Route::post('change_password', 'UserApi\AccountApiController@change_password'); // 3

        Route::post('delete_account', 'UserApi\AccountApiController@delete_account'); // 4

        Route::post('logout', 'UserApi\AccountApiController@logout'); // 7

        Route::post('push_notification_update', 'UserApi\AccountApiController@push_notification_status_change');  // 5

        Route::post('email_notification_update', 'UserApi\AccountApiController@email_notification_status_change'); // 6
        
        Route::post('dashboard', 'UserApi\AccountApiController@dashboard'); // 6

    });

    Route::post('project/configurations', 'ApplicationController@configurations'); 

    // Cards management start

    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('cards_add', 'UserApi\AccountApiController@cards_add'); // 15

        Route::post('cards_list', 'UserApi\AccountApiController@cards_list'); // 16

        Route::post('cards_delete', 'UserApi\AccountApiController@cards_delete'); // 17

        Route::post('cards_default', 'UserApi\AccountApiController@cards_default'); // 18

        Route::post('payment_mode_default', 'UserApi\AccountApiController@payment_mode_default');

    });

    // Subscriptions management start

    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('subscriptions_index', 'UserApi\AccountApiController@subscriptions_index');

        Route::post('subscriptions_view', 'UserApi\AccountApiController@subscriptions_view');

        Route::post('subscriptions_payment_by_stripe', 'UserApi\AccountApiController@subscriptions_payment_by_stripe');

        Route::post('subscriptions_history', 'UserApi\AccountApiController@subscriptions_history');

        Route::post('subscriptions_current_plan', 'UserApi\AccountApiController@subscriptions_current_plan');

        Route::post('subscriptions_autorenewal_enable', 'UserApi\AccountApiController@subscriptions_autorenewal_enable');
        
        Route::post('subscriptions_autorenewal_cancel', 'UserApi\AccountApiController@subscriptions_autorenewal_cancel');

    });

    // Meeting management end

    Route::post('meetings_join', 'UserApi\MeetingApiController@meetings_join');
    
    Route::post('meetings_join_jitsi', 'UserApi\MeetingApiController@meetings_join_jitsi');

    Route::any('meeting_records_save', 'UserApi\MeetingApiController@meeting_records_save')->name('meeting_records_save');

    Route::any('meetings_end', 'UserApi\MeetingApiController@meetings_end')->name('meetings_end');
    
    Route::any('meetings_users_logout', 'UserApi\MeetingApiController@meetings_users_logout')->name('meetings_users_logout');

    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('meetings_index', 'UserApi\MeetingApiController@meetings_index');

        Route::post('meetings_upcoming', 'UserApi\MeetingApiController@meetings_upcoming');

        Route::post('meetings_view', 'UserApi\MeetingApiController@meetings_view');

        Route::post('meetings_search', 'UserApi\MeetingApiController@meetings_search');

        Route::post('meetings_save', 'UserApi\MeetingApiController@meetings_save');
        
        Route::post('meetings_now_save', 'UserApi\MeetingApiController@meetings_now_save');

        Route::post('meetings_save_jitsi', 'UserApi\MeetingApiController@meetings_save_jitsi');

        Route::post('meetings_delete', 'UserApi\MeetingApiController@meetings_delete');


        Route::post('meetings_vod_video_save', 'UserApi\MeetingApiController@meetings_vod_video_save');

        Route::post('meetings_live_start', 'UserApi\MeetingApiController@meetings_live_start');

        Route::post('meetings_live_end', 'UserApi\MeetingApiController@meetings_live_end');
        
        // Route::post('meetings_end', 'UserApi\MeetingApiController@meetings_end');

        Route::post('meeting_users_index', 'UserApi\MeetingApiController@meeting_users_index');

        Route::post('meeting_users_search', 'UserApi\MeetingApiController@meeting_users_search');
    });

    Route::group(['middleware' => 'UserApiVal'] , function() {

        Route::post('meeting_records_index', 'UserApi\MeetingApiController@meeting_records_index');

        Route::post('meeting_records_upcoming', 'UserApi\MeetingApiController@meeting_records_upcoming');

        Route::post('meeting_records_view', 'UserApi\MeetingApiController@meeting_records_view');

        Route::post('meeting_records_search', 'UserApi\MeetingApiController@meeting_records_search');

        Route::post('meeting_records_delete', 'UserApi\MeetingApiController@meeting_records_delete');
    });

});

